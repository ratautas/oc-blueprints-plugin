<?php

use RainLab\Translate\Models\Locale;
use RainLab\Pages\Classes\Page;
use Cms\Classes\Theme;

function createRoute($route)
{
    // Make a dynamic URL (/LANGUAGE/api):
    Route::get($route, function () {
        // Get active theme:
        $theme = Theme::getActiveThemeCode();
        // Get all static pages:
        $static_pages = Page::listInTheme($theme, false);
        // Create a new empty object:
        $pages = new stdClass();
        //Loop through all the static pages and assign $pages object it's viewbag and content.
        foreach ($static_pages as $name => $page_object) {
            $pages->$name['content'] = $page_object['content'];
            $pages->$name = $page_object['viewBag'];
        }
        // Return all $pages as JSON object. Easy
        return Response::json($pages);
    });
}

// Check if 'RainLab\Translate' is installed
if (class_exists('RainLab\Translate\Models\Locale')) {
    // If it is installed, loop through all available languages:
    foreach (Locale::listEnabled() as $language => $title) {
        return createRoute('/api/'.$language);
    }
} else {
    // Just create a simple endpoint.
    return createRoute('/api');
}
